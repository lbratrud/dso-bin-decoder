#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>

#include <sys/file.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>

/* Status message macros */
#define INFO(format, args...) \
	fprintf (stdout, "" format, ## args)
#define ERROR(format, args...) \
	fprintf (stderr, "Error: " format, ## args)

bool debug = false;

/* 164 bytes */
struct tHeader_s {
/* All string types are nullterminated in struct, but in file is not.
 * This is indicated with +1 in field
 * */
  unsigned char A[2+1];
  unsigned char version[2+1];
  uint32_t size;
  uint32_t num_waveforms;
  struct WaveformHeader {
    uint32_t size;
    uint32_t type;
    uint32_t num_buffers;
    uint32_t points;
    uint32_t count;
    float x_range;
    double x_orig;
    double x_incr;
    double x_disp_orig;
    uint32_t x_units; // 0,1,2,3,4,5
    uint32_t y_units;
    /* Time stamp */
    unsigned char date[16+1];
    unsigned char time[16+1];
    unsigned char frame[24+1]; // MODEL#:SERIAL#
    unsigned char wvf_label[16+1];
    //
    double time_tags;
    uint32_t seg_idx;
  } wvf_hd;
  /* Data header*/
  struct WaveformDataHeader {
    int32_t size;
    uint16_t buf_type; // 0,1,2,3,4,5,6
    uint16_t bytes_pr_point;
    uint32_t buf_size;
  } wvf_dat_hd;
} tHeaderDefault = {0};

typedef struct tHeader_s tHeader; 

char str_x_units[6][9] = {"Unknown", "Volt", "Second", "Constant", "Amp", "Decibel"};
char str_y_units[6][9]  = {"Unknown", "Volt", "Second", "Constant", "Amp", "Decibel"};

FILE * inFile;
long inFileSize;
/* Functions */
void print_help(void)
{
  INFO("----------------------------------------------------------------------------------------------\n");
	INFO("Usage: bin-decode [options]\n");
  INFO("----------------------------------------------------------------------------------------------\n");
	INFO("\n");
	INFO("Options:\n");
	INFO("--input-file,i       <filename>.bin                    Input filename\n");
	INFO("--output-file,o      <filename>.csv                    Output filename\n");
	INFO("--peak,p             {i[ndex]}{,v[alue]}[:<filename>]  Output file containing information about peak \n");
  INFO("                                                       Use: -p i[ndex] filename for index of peak only\n");
  INFO("                                                            -p v[alue] filename for peak value only\n");
  INFO("                                                            -p i[index],v[alue] filename for both\n");
  INFO("                                                       If no filename is defined, the result is displayed on stdout\n");
	INFO("--help,h                                               Display help\n");
}

void decode(char * filename, char * out_file, bool peak_find, bool peak_only);
static int parse_options(int argc, char *argv[]);
uint32_t peak_finder(void * data, uint32_t arr_size, float min_peak_height);

char * inputFilename;
bool inFileDefined = false;
bool outFileDefined = false;
char * outputFilename;

/* Peak finder stuff */
bool peakFind = false;
bool findPeakOnly = false;
bool peakFileDefined = false;
bool peakIndexDef = false;
bool peakValueDef = false;
char * peakFilename;

/* Here starts the shit TODO: Tons of memory leaks */
int main(int argc, char * argv[])
{
	parse_options(argc, argv);
  
  if(!outFileDefined && !peakFind){
//    outputFilename = (char *) malloc(strlen("out.csv")+1);
//    strcpy(outputFilename, "out.csv");
    printf("No filename was defined and you don't want to find peaks, exiting...\n");
    return 0;
  }
  
  if(!outFileDefined && peakFind) { findPeakOnly = true; }
  
  if(!inFileDefined) {printf("No input defined, exiting...\n"); return 0; }

	if(inFileDefined) { decode(inputFilename, outputFilename, peakFind, findPeakOnly); }

  return 0;
}

/* Functions */
void decode(char * filename, char * out_file, bool peak_find, bool peak_only)
{
  /* Open file */
  inFile = fopen(filename, "rb");
  if(inFile == NULL) {
    fprintf(stdout, "Read: Error opening file %s, errno: %s\n", optarg, strerror(errno));
    exit(1);
  }

  /* Check filesize */
  fseek(inFile, 0L, SEEK_END);
  inFileSize = ftell(inFile); 
  rewind(inFile);

  /* Read file header */
  tHeader header = tHeaderDefault;
  size_t res;
  res = fread(&header.A, sizeof(uint8_t), 2, inFile);
  res = fread(&header.version, sizeof(uint8_t), 2, inFile);
  res = fread(&header.size, sizeof(uint32_t), 1, inFile);
    if(inFileSize != header.size) {
      fprintf(stderr, "Header file size (%d) did not match file size read by fopen (%ld)\n", header.size, inFileSize);
      exit(2);
    }
  res = fread(&header.num_waveforms, sizeof(uint32_t), 1, inFile);
  if(debug) {
    printf("================\nFile Header:\n================\n");
    printf("header.A: %c%c\n", header.A[0], header.A[1]);
    printf("header.version: %c%c\n", header.version[0], header.version[1]);
    printf("header.size %d\n", header.size);
    printf("header.num_waveforms: %d\n", header.num_waveforms);
  }

  /* Read Waveform header */
  res = fread(&header.wvf_hd.size, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_hd.type, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_hd.num_buffers, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_hd.points, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_hd.count, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_hd.x_range, sizeof(uint32_t), 1, inFile); // float
  res = fread(&header.wvf_hd.x_disp_orig, sizeof(uint64_t), 1, inFile); // double
  res = fread(&header.wvf_hd.x_incr, sizeof(uint64_t), 1, inFile); // double
  res = fread(&header.wvf_hd.x_orig, sizeof(uint64_t), 1, inFile); // double
  res = fread(&header.wvf_hd.x_units, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_hd.y_units, sizeof(uint32_t), 1, inFile);
  /* Time stamp */
  res = fread(header.wvf_hd.date, sizeof(uint8_t), 16, inFile);
  res = fread(header.wvf_hd.time, sizeof(uint8_t), 16, inFile);
  res = fread(header.wvf_hd.frame, sizeof(uint8_t), 24, inFile);
  res = fread(header.wvf_hd.wvf_label, sizeof(uint8_t), 16, inFile);
  res = fread(&header.wvf_hd.time_tags, sizeof(uint64_t), 1, inFile);
  res = fread(&header.wvf_hd.wvf_label, sizeof(uint32_t), 1, inFile);

  if(debug) {
    printf("================\nWaveform Header:\n================\n");
    printf("header.wvf_hd.size: %d\n", header.wvf_hd.size);
    printf("header.wvf_hd.type: %d\n", header.wvf_hd.type);
    printf("header.wvf_hd.num_buffers: %d\n", header.wvf_hd.num_buffers);
    printf("header.wvf_hd.points: %d\n", header.wvf_hd.points);
    printf("header.wvf_hd.count: %d\n", header.wvf_hd.count);
    printf("header.wvf_hd.x_range: %.7f\n", header.wvf_hd.x_range);
    printf("header.wvf_hd.x_disp_orig: %.15f\n", header.wvf_hd.x_disp_orig);
    printf("header.wvf_hd.x_incr: %.15f\n", header.wvf_hd.x_incr);
    printf("header.wvf_hd.x_orig: %.15f\n", header.wvf_hd.x_orig);
    printf("header.wvf_hd.x_units: %d\n", header.wvf_hd.x_units);
    printf("header.wvf_hd.y_units: %d\n", header.wvf_hd.y_units);
    printf("header.wvf_hd.date: %s\n", header.wvf_hd.date);
    printf("header.wvf_hd.time: %s\n", header.wvf_hd.time);
    printf("header.wvf_hd.frame: %s\n", header.wvf_hd.frame);
    printf("header.wvf_hd.wvf_label: %s\n", header.wvf_hd.wvf_label);
    printf("header.wvf_hd.time_tags: %.15f\n", header.wvf_hd.time_tags);
    printf("header.wvf_hd.wvf_idx: %d\n", header.wvf_hd.seg_idx);
  }
  
  /* Waveform Data Header */
  res = fread(&header.wvf_dat_hd.size, sizeof(uint32_t), 1, inFile);
  res = fread(&header.wvf_dat_hd.buf_type, sizeof(uint16_t), 1, inFile);
  res = fread(&header.wvf_dat_hd.bytes_pr_point, sizeof(uint16_t), 1, inFile);
  res = fread(&header.wvf_dat_hd.buf_size, sizeof(uint32_t), 1, inFile);

  if(debug) {
    printf("================\nWaveform Data Header:\n================\n");
    printf("header.wvf_dat_hd.size: %d\n", header.wvf_dat_hd.size);
    printf("header.wvf_dat_hd.buf_type: %d\n", header.wvf_dat_hd.buf_type);
    printf("header.wvf_dat_hd.bytes_pr_point: %d\n", header.wvf_dat_hd.bytes_pr_point);
    printf("header.wvf_dat_hd.buf_size: %d\n", header.wvf_dat_hd.buf_size);
  }

  /* Put Data in buffer */
  float * data_buffer;
  if(header.wvf_dat_hd.buf_type == 1) {
    data_buffer = (float*) calloc(header.wvf_dat_hd.buf_size, sizeof(float));
    res = fread(data_buffer, sizeof(float), header.wvf_dat_hd.buf_size, inFile);

    if(sizeof(float)*res != header.wvf_dat_hd.buf_size) printf("Buffer was not read correctly? res=%ld\n", res);
  }

  /* Find the peak */
  uint32_t peak_idx = peak_finder(data_buffer,header.wvf_dat_hd.buf_size/header.wvf_dat_hd.bytes_pr_point, 0.5);
  float peak = *(data_buffer+peak_idx);

  if( peak_only == false ) {
    FILE * fCsv;
    fCsv = fopen(out_file, "w");
    fprintf(fCsv, "#%s\n", header.wvf_hd.wvf_label);
    fprintf(fCsv,"#%s %s %s\n", header.wvf_hd.date, header.wvf_hd.time, header.wvf_hd.frame);
    fprintf(fCsv, "#Peak index, Count, X Display Range, X Display Origin, X Increment, X Origin, X Units, Y Units\n");
    fprintf(fCsv, "%d,%d,%.7f,%.15f,%.15f,%.15f,%s,%s\n", peak_idx, header.wvf_hd.count,header.wvf_hd.x_range,header.wvf_hd.x_orig,header.wvf_hd.x_incr,
                  header.wvf_hd.x_disp_orig,str_x_units[header.wvf_hd.x_units], str_y_units[header.wvf_hd.y_units]);
    for(int i=0;i<res;i++) {
      if(i<res-1) {
        fprintf(fCsv, "%.7f,", *(data_buffer+i));
      } else {
        fprintf(fCsv, "%.7f", *(data_buffer+i));
      }
    }
    fclose(fCsv);
  }
  /* Output peak */
  if(peak_find){
  FILE * fPeak;
    if(peakFileDefined) { fPeak = fopen(peakFilename, "w"); }
      if( peakIndexDef ) { fprintf( (peakFileDefined ? fPeak : stdout), "%d", peak_idx); }
      if( peakIndexDef && peakValueDef ) { fprintf( (peakFileDefined ? fPeak : stdout), ","); }
      if( peakValueDef ) { fprintf( (peakFileDefined ? fPeak : stdout), "%.7f", peak); }
      if( peakIndexDef || peakValueDef ) { fprintf( (peakFileDefined ? fPeak : stdout), "\n"); }
    if(peakFileDefined) {fclose(fPeak);}
  }
  fclose(inFile);
  if(debug) printf("peak_finder: %d\n", peak_finder(data_buffer,header.wvf_dat_hd.buf_size/header.wvf_dat_hd.bytes_pr_point, 0.5));
}

static int parse_options(int argc, char *argv[])
{
	static int c;
  /* Print usage help if no arguments */
	if (argc == 1)
	{
		print_help();
		exit(1);
	}

	while (1)
	{
		static struct option long_options[] =
		{
			{"debug",	  no_argument,		    0, 'd'},
			{"input-file", required_argument,	0, 'i'},
			{"output-file", required_argument,	0, 'o'},
			{"peak", required_argument,	0, 'p'},
			{"help",	  no_argument,		    0, 'h'},
			{0, 0, 0, 0}
		};
		
		/* getopt_long stores the option index here. */
		int option_index = 0;

		/* Parse argument using getopt_long (no short opts allowed) */
		c = getopt_long (argc, argv, "di:o:p:h", long_options, &option_index);
		//c = getopt_long (argc, argv, "i:n:p:s:f:a:t:d:v:h:", long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c)
		{
			case 0:
				/* If this option set a flag, do nothing else now. */
				if (long_options[option_index].flag != 0)
					break;
				INFO("option %s", long_options[option_index].name);
				if (optarg)
					INFO(" with arg %s", optarg);
				INFO("\n");
				break;

      /* Print debug output */
      case 'd':
        debug = true;
        break;

      /* Output peak to file */
      case 'p':
        /* Extract information from optarg first */
        {} // fucking case
        char * options = (char *) calloc(strlen(optarg) + 1, sizeof(char));
        strcpy(options, optarg);
         
        /* Extract first part (options) */
        int n = 0;
        while( (*(options+n) != ':') && (*(options+n) != '\0')) { n++; }
        /* Extract filename if it was defined */
        if( (*(optarg+n) == ':') && (*(optarg+n) != '\0')) {  
          *(options+n) = '\0';
          peakFilename = strdup(&*(optarg+n+1));
          peakFileDefined = true;
        } else { 
          peakFileDefined = false;
        }
        /* Check the options */
        char * token;
        while(( token = strsep(&options, ",")) != NULL) {
          if( (strcmp(token, "index") == 0) || (strcmp(token, "i") == 0) ) { peakIndexDef = true; }
          if( (strcmp(token, "value") == 0) || (strcmp(token, "v") == 0) ) { peakValueDef = true; }
        }
//        free(token);
        peakFind = true;
        free(options);
        break;

      /* Decode input file */
			case 'i':
        inputFilename = (char *) malloc(strlen(optarg) + 1);
        strcpy(inputFilename, optarg);
				inFileDefined = true;
				break;

      case 'o':
        outputFilename = (char *) malloc(strlen(optarg) + 1);
        strcpy(outputFilename, optarg);
        outFileDefined = true;
        break;

      /* Print help */
			case 'h':
				print_help();
				exit(0);
				break;
			
      case '?':
				/* getopt_long already printed an error message. */
				break;
			default:
				ERROR("End of options\n");
				exit(1);
		}
	}

	/* Print any remaining command line arguments (invalid options). */
	if (optind < argc)
	{
		ERROR("%s: unknown arguments: ", argv[0]);
		while (optind < argc)
			ERROR("%s ", argv[optind++]);
		ERROR("\n");
		exit(1);
	}
  return 0;
}
uint32_t peak_finder(void * data, uint32_t arr_size, float min_peak_height)
{
  uint32_t ret = -1;
  uint32_t mid = arr_size/2;
  float current_peak = 0.0;
  
  if(debug) {
    printf("%.7f\n", *((float*)(data)+mid));
    printf("%.7f\n", fabs(*((float*)(data)+mid)));
    printf("%.7f\n", fabs(*((float*)(data)+mid-1)));
    printf("%.7f\n", fabs(min_peak_height));
  }
  /* XXX: (value[n]>threshold&&value[n+1]>threshold) ? n++ : n+2;  */
  for(int n = 1; n<=(arr_size-1); n++) {
    if(debug) printf("%d:%.7f, ",n,fabs(*((float*)(data)+n)));
    if( (fabs(*((float*)(data)+n)) > fabs(*((float*)(data)+n+1))) 
    &&  (fabs(*((float*)(data)+n)) > fabs(*((float*)(data)+n-1)))
    //&&  (fabs(*((float*)(data)+n)) > fabs(min_peak_height) )) {
    &&  (fabs(*((float*)(data)+n)) > current_peak )) {
      /* Found peak */
      current_peak = fabs(*((float*)(data)+n));
      if(debug) printf("found peak: %d\n", n);
      ret = n;
    }
  }

  return ret;
}
