head = csvread("out.csv", [3 0 3 5]); % Read information from CSV header
data = csvread("out.csv", 4, 0); % Read data
% Extract some stuff from the header
x_range  = head(3);
x_step   = head(5);
x_origin = head(4);

fileID = fopen('out.csv');
% Skip comments
fgetl(fileID);
fgetl(fileID);
fgetl(fileID);
% Extract Header
header = textscan(fileID, '%d%d%f%f%f%f%s%s',1,'delimiter',',');
fclose(fileID);
a = x_origin:x_step:(x_origin+x_range-x_step);
peak_idx = header{1}; 
plot(a, data);
hold on;
plot(a(peak_idx+1), data(peak_idx+1), 'b*');
xlabel(header{1,7}{1});
ylabel(header{1,8}{1});
