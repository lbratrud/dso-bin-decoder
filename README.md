# Decoder for bin files captured with Agilent DSO

Contact: Lars Bratrud <lars.bratrud@cern.ch>

## Compilation
```
git clone ssh://git@gitlab.cern.ch:7999/lbratrud/dso-bin-decoder.git
cd dso-bin-decoder
make
```

## Use
```
./bin-decode -h                                 # Print help
./bin-decode -i dataset.bin -o out.csv          # Decode file
./bin-decode -i dataset.bin -p value:peak.file  # Write only the peak to a file
./bin-decode -i dataset.bin -p i,v:peak.file    # Write the index of the peak and it's value to a file
```
The decoder can be used to decode bin-files from Agilent oscilloscopes and also to find waveform peaks.
Omitting the filename in the peak finder option will output the data to stdout.

## Output fileformat
The output is a comma separated file.
First three lines are comments. First line has the waveform label, second line has time and equipment identification
and the third line describes the header of the CSV.
The fourth line is the header as a comma separated vector of a length of 8. 
```
#Waveform label
#27 APR 2017 15:35:27 DSO9254A:MY53020107
#Peak index, Count, X Display Range, X Display Origin, X Increment, X Origin, X Units, Y Units
69,1,0.0000029,-0.000001412214046,0.000000020000000,-0.000001413100041,Second,Volt
0.0217457,-0.0235701,0.0193745,0.0029080,0.0168716,-0.0035468,-0.0501799,-0.0329231,-0.0549223,0.005937, ...
```
Index of peak counts from 0.
