all: bin-decode.c
	gcc -o bin-decode bin-decode.c

.PHONY: clean

clean:
	rm bin-decode
